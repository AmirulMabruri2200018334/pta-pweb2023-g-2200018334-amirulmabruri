<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <title>Kost Amirul</title>
    <link rel="icon" href="assets/logo.png">
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
</head>
<body>
    <div class="container">
        <header>
            <h1>Kost Amirul</h1>
            <p>Tempat nyaman dan murah untuk tinggal</p>
        </header>
        <nav>
            <ul>
                <li><a href="index.php">Beranda</a></li>
                <li><a href="paket.php">Paket</a></li>
                <li><a href="booking.php">Booking</a></li>
            </ul>
        </nav>
        <main>
            <h2>Selamat Datang di Kost Amirul</h2>
            <p>Kost-Kostan Amirul adalah tempat yang cocok untuk Anda yang mencari tempat tinggal yang nyaman, bersih, dan murah. Kami menyediakan berbagai pilihan paket kamar dengan fasilitas yang lengkap dan harga yang terjangkau. Anda juga bisa memesan kamar secara online melalui website ini dengan mudah dan cepat.</p>
            <p>Silakan pilih paket yang sesuai dengan kebutuhan dan budget Anda di halaman Paket. Jika Anda sudah menentukan pilihan Anda, silakan isi form booking di halaman Booking. Kami akan segera menghubungi Anda untuk konfirmasi dan pembayaran.</p>
            <p>Terima kasih telah memilih Kost sebagai tempat tinggal Anda. Kami tunggu kedatangan Anda.</p>
        </main>
        <footer>
            <p>© 2023 Amirul Mabruri. All rights reserved.</p>
        </footer>
    </div>
</body>
</html>
