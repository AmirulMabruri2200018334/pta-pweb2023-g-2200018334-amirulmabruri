<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <title>Kost Amirul</title>
    <link rel="icon" href="assets/logo.png">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
<header>
<h1>Kost Amirul</h1>
<p>Tempat nyaman dan murah untuk tinggal</p>
</header>
<nav>
<ul>
<li><a href="index.php">Beranda</a></li>
<li><a href="paket.php">Paket</a></li>
<li><a href="booking.php">Booking</a></li>
</ul>
</nav>
<main>
<h2>Paket Kamar yang Tersedia</h2>
<p>Berikut adalah daftar paket kamar yang kami tawarkan, beserta harga dan fasilitasnya. Silakan pilih paket yang sesuai dengan kebutuhan dan budget Anda.</p>

<?php

$paket = array(
  array("nama_paket" => "Paket A", "harga" => "Rp. 500.000", "fasilitas" => "AC, TV, Kamar Mandi Dalam", "foto" => "assets/kos1.jpg"),
  array("nama_paket" => "Paket B", "harga" => "Rp. 400.000", "fasilitas" => "AC, TV, Kamar Mandi Luar", "foto" => "assets/kos2.jpg"),
  array("nama_paket" => "Paket C", "harga" => "Rp. 300.000", "fasilitas" => "Kipas Angin, TV, Kamar Mandi Luar", "foto" => "assets/kos3.jpg"),
  array("nama_paket" => "Paket D", "harga" => "Rp. 600.000", "fasilitas" => "AC, TV, Kamar Mandi Dalam, Wifi", "foto" => "assets/kos4.jpeg"), 
  array("nama_paket" => "Paket E", "harga" => "Rp. 700.000", "fasilitas" => "AC, TV, Kamar Mandi Dalam, Wifi, Balkon", "foto" => "assets/kos5.jpg") 
);

echo "<table>";
echo "<tr>";
echo "<th>Nama Paket</th>";
echo "<th>Harga per Bulan</th>";
echo "<th>Fasilitas</th>";
echo "<th>Foto</th>"; 
echo "</tr>";


foreach ($paket as $row) {

  
  echo "<tr>";
  echo "<td>" . $row["nama_paket"] . "</td>";
  echo "<td>" . $row["harga"] . "</td>";
  echo "<td>" . $row["fasilitas"] . "</td>";
  echo "<td><img src='" . $row["foto"] . "' alt='" . $row["nama_paket"] . "' width='200'></td>"; 
  echo "</tr>";
}

echo "</table>";

?>

</main>
<footer>
<p>© 2023 Amirul Mabruri. All rights reserved.</p>
</footer>
</div>
</body>
</html>
