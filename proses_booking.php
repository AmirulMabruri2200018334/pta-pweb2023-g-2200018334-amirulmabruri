<?php


$nama = $_POST["nama"];
$email = $_POST["email"];
$telepon = $_POST["telepon"];
$paket = $_POST["paket"];
$tanggal = $_POST["tanggal"];


$data = "Nama: $nama\nEmail: $email\nTelepon: $telepon\nPaket: $paket\nTanggal: $tanggal\n\n";


file_put_contents("booking.txt", $data, FILE_APPEND);

?>

<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <title>Kost Ayu - Booking</title>
    <link rel="icon" href="assets/logo.png">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
<header>
<h1>Kost Amirul</h1>
<p>Tempat nyaman dan murah untuk tinggal</p>
</header>
<nav>
<ul>
<li><a href="index.html">Beranda</a></li>
<li><a href="paket.html">Paket</a></li>
<li><a href="booking.html">Booking</a></li>
</ul>
</nav>
<main>

<h2>Terima Kasih Telah Memesan Kamar di Kost Amirul</h2>

<p>Data booking Anda telah kami terima dan kami simpan di file booking.txt. Kami akan segera menghubungi Anda untuk konfirmasi dan pembayaran.</p>

<p>Berikut adalah data booking Anda:</p>

<ul>
<li>Nama: <?php echo $nama; ?></li>
<li>Email: <?php echo $email; ?></li>
<li>Telepon: <?php echo $telepon; ?></li>
<li>Paket: <?php echo $paket; ?></li>
<li>Tanggal: <?php echo $tanggal; ?></li>
</ul>

</main>
<footer>
<p>© 2023 Amirul Mabruri. All rights reserved.</p>
</footer>
</div>
</body>
</html>

