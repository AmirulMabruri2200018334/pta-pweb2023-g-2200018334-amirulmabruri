
function validateForm() {
    
}

var form = document.getElementById("booking-form");

form.addEventListener("submit", function(event) {
    
    var nama = document.getElementById("nama").value;
    var email = document.getElementById("email").value;
    var telepon = document.getElementById("telepon").value;
    var paket = document.getElementById("paket").value;
    var tanggal = document.getElementById("tanggal").value;

    var error = "";

    if (nama == "") {
        error += "Nama harus diisi.\n";
    }

    if (email == "") {
        error += "Email harus diisi.\n";
    } else if (!email.includes("@") || !email.includes(".")) {
        error += "Email tidak valid.\n";
    }

    if (telepon == "") {
        error += "Telepon harus diisi.\n";
    } else if (isNaN(telepon)) {
        error += "Telepon harus berupa angka.\n";
    }


    if (paket == "") {
        error += "Paket harus dipilih.\n";
    }

    if (tanggal == "") {
        error += "Tanggal harus diisi.\n";
    } else {
        
        var today = new Date();
        var year = today.getFullYear();
        var month = today.getMonth() + 1;
        var day = today.getDate();
        if (month < 10) {
            month = "0" + month;
        }
        if (day < 10) {
            day = "0" + day;
        }
        var todayDate = year + "-" + month + "-" + day;

        if (tanggal < todayDate) {
            error += "Tanggal tidak boleh sebelum hari ini.\n";
        }
    }

    if (error != "") {
        alert("Mohon perbaiki kesalahan berikut:\n" + error);
        event.preventDefault();
    }
});
